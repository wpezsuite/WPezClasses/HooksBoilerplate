## WPezClasses: ClassHooks boilerplate

__Boilerplate for encapsulating hooks for WPezClasses The ezWay.__
   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

Note: The callbacks listed in the hooks should also be methods in the Interface.

### FAQ

__1) Why?__

When appropriate, it makes sense to supply a preconfigured hooks file. This the ez pattern for that. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 

### HELPFUL LINKS

n/a

### TODO

n/a

### CHANGE LOG

- v0.0.2 - Monday 22 Aril 2019
    - UPDATED: interface file / name

- v0.0.1 - Tuesday 16 April 2019
    - Hey! Ho!! Let's go!!!